import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DossiersLijstComponent } from './components/dossiers-lijst/dossiers-lijst.component';

const dossiersRoutes: Routes = [
  { path: '', component: DossiersLijstComponent },
  { path: ':dossierId', loadChildren: './dossier/dossier.module#DossierModule' }
];

@NgModule({
  imports: [
    RouterModule.forChild(dossiersRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DossiersRoutingModule { }
