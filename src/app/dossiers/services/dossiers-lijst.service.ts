
import {throwError as observableThrowError, Observable} from 'rxjs';

import {catchError, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';


import {DossiersLijstResponse} from '../models/dossiers-lijst-response';
import {Status} from '../../common/models/status';
import {School} from '../../common/models/school';

// const APIURL = '/assets/data2.json';
const APIURL = '/api/candidates';

@Injectable()
export class DossiersLijstService {

  // Error loggen en vervolgens de stacktrace verder gooien
  private static onError(res: Response): Observable<any> {
    const error = `Error $res.status: ${res}`;
    console.error('ERROR: ' + error);
    return observableThrowError(error);
  }

  constructor(private http: HttpClient) {
  }

  // Api aanspreken, mappen en errors opvangen
  getCandidateData(): Observable<DossiersLijstResponse[]> {
    return this.http.get(APIURL).pipe(
      map(this.mapCandidateData),
      catchError(DossiersLijstService.onError),);
  }

  // Personen per dossier opvangen
  private mapCandidateData(candidatesJson: any[]): DossiersLijstResponse[] {
    const kandidaatLijstResponse: DossiersLijstResponse[] = [];
    candidatesJson.forEach(function (candidate) {
      const candidateRequests = candidate.CandidateRequests;
      if (candidateRequests.length > 0) {
        candidateRequests.forEach(function (candidateRequest) {
          kandidaatLijstResponse.push({
            id: candidateRequest.Id,
            dossiernummer: candidateRequest.ReferenceNumber,
            voornaam: candidate.Firstname,
            achternaam: candidate.Lastname,
            registratiedatum: candidateRequest.RegisterDate,
            status: Status[candidateRequest.Status],
            school: School[candidateRequest.School]
        });
        });
      }
    });
    return kandidaatLijstResponse;
  }

}
