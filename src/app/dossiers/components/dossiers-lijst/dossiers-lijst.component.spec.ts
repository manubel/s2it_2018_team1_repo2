import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DossiersLijstComponent } from './dossiers-lijst.component';

describe('DossiersLijstComponent', () => {
  let component: DossiersLijstComponent;
  let fixture: ComponentFixture<DossiersLijstComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DossiersLijstComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DossiersLijstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
