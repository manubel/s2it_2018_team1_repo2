import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DossiersLijstTabelComponent } from './dossiers-lijst-tabel.component';

describe('DossiersLijstTabelComponent', () => {
  let component: DossiersLijstTabelComponent;
  let fixture: ComponentFixture<DossiersLijstTabelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DossiersLijstTabelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DossiersLijstTabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
