import {Component, OnInit, ViewChild, ViewChildren} from '@angular/core';
import {MatSort, MatTableDataSource, MatPaginator} from '@angular/material';
import {DossiersLijstResponse} from '../../../models/dossiers-lijst-response';
import {DossiersLijstService} from '../../../services/dossiers-lijst.service';
import {Status} from '../../../../common/models/status';
import {School} from '../../../../common/models/school';

@Component({
  selector: 'dossier-lijst-tabel',
  templateUrl: './dossiers-lijst-tabel.component.html',
  styleUrls: ['./dossiers-lijst-tabel.component.scss']
})
export class DossiersLijstTabelComponent implements OnInit {

  statusDropdown = Status;
  statusSelected: Number;

  schoolDropdown = School;
  schoolSelected: Number;

  displayedColumns = ['dossiernummer', 'voornaam', 'achternaam', 'registratiedatum', 'status', 'school'];
  displayedFilters = ['dossiernummerFilter', 'voornaamFilter', 'achternaamFilter', 'registratiedatumFilter', 'statusFilter', 'schoolFilter'];
  dataSource: MatTableDataSource<DossiersLijstResponse>;

  lastSelectedFilterField = '';

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChildren('clearableField') clearableFields: any;

  constructor(private _service: DossiersLijstService) {
  }

  ngOnInit() {
    this._service.getCandidateData().subscribe(
      data => {
        this.dataSource = new MatTableDataSource<DossiersLijstResponse>(data);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      }
    );
  }

  applyFilter(filterValue: any, fieldName: string) {
    // In the case of dropdown, when value is not given (no filter option) then set table back to default
    if (!filterValue) {
      this.dataSource.filter = null;
      return;
    }
    // Only set predicate when it is not on the correct field
    if (this.lastSelectedFilterField !== fieldName) {
      this.applyFilterPredicate(fieldName);
    }

    // Set filter value for dropdowns
    if (fieldName === 'status') {
      filterValue = Status[filterValue];
    } else if (fieldName === 'school') {
      filterValue = School[filterValue];
    }

    // Do the filtering
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    filterValue = filterValue.replace(/-/g, '');
    this.dataSource.filter = filterValue;
  }

  // Sets the predicate for the table, this is so the table knows which field to filter on
  private applyFilterPredicate(fieldName: string) {
    this.lastSelectedFilterField = fieldName;
    switch (fieldName) {
      case 'dossiernummer': {
        this.dataSource.filterPredicate =
          (data: DossiersLijstResponse, filter: string) =>
            data.dossiernummer.trim().toLowerCase().indexOf(filter) !== -1;
        break;
      }
      case 'voornaam': {
        this.dataSource.filterPredicate =
          (data: DossiersLijstResponse, filter: string) =>
            data.voornaam.trim().toLowerCase().indexOf(filter) !== -1;
        break;
      }
      case 'achternaam': {
        this.dataSource.filterPredicate =
          (data: DossiersLijstResponse, filter: string) =>
            data.achternaam.trim().toLowerCase().indexOf(filter) !== -1;
        break;
      }
      case 'registratieDatum': {
        this.dataSource.filterPredicate =
          (data: DossiersLijstResponse, filter: string) =>
            data.registratiedatum.trim().toLowerCase().indexOf(filter) !== -1;
        break;
      }
      case 'status': {
        this.dataSource.filterPredicate =
          (data: DossiersLijstResponse, filter: string) =>
            data.status.toLowerCase().indexOf(filter) !== -1;
        break;
      }
      case 'school': {
        this.dataSource.filterPredicate =
          (data: DossiersLijstResponse, filter: string) =>
            data.school.toLowerCase().indexOf(filter) !== -1;
      }
    }
  }

  clearTable(fieldName: string) {
    if (this.lastSelectedFilterField === fieldName || this.dataSource.filter === null) {
      return;
    }
    this.dataSource.filter = null;
    if (fieldName === 'status') {
      this.schoolSelected = 0;
    } else if (fieldName === 'school') {
      this.statusSelected = 0;
    } else {
      this.schoolSelected = 0;
      this.statusSelected = 0;
    }

    // Set each inputfield to empty except for itself
    this.clearableFields.forEach(item => {
      if (item.nativeElement.localName !== fieldName) {
        item.nativeElement.value = null;
      }
    });
  }
}
