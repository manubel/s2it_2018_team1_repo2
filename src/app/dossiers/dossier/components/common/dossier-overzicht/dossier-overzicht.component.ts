import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject ,  Subject } from 'rxjs';
import { Status } from '../../../../../common/models/status';

@Component({
  selector: 'dossier-overzicht',
  templateUrl: './dossier-overzicht.component.html',
  styleUrls: ['./dossier-overzicht.component.scss']
})
export class DossierOverzichtComponent implements OnInit {

  @Input() candidateRequestDetails$: BehaviorSubject<any>;
  @Input() candidateRequestDetailsHasError$: Subject<boolean>;

  constructor() { }

  ngOnInit() {
  }

  getStatus(status: number): string {
    return Status[status];
  }

}
