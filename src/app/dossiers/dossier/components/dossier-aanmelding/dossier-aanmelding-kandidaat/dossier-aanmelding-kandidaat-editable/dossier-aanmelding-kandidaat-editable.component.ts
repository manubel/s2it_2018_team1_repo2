import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Degree } from '../../../../../../common/models/degree';

@Component({
  selector: 'dossier-aanmelding-kandidaat-editable',
  templateUrl: './dossier-aanmelding-kandidaat-editable.component.html',
  styleUrls: ['./dossier-aanmelding-kandidaat-editable.component.scss']
})
export class DossierAanmeldingKandidaatEditableComponent implements OnInit {

  public kandidaatForm: FormGroup;
  public degrees = Degree;

  constructor(private formBuilder: FormBuilder) { }

  @Input() candidateRequestDetails$: Observable<any>;
  @Input() editing$: Observable<boolean>;

  ngOnInit() {
    this.candidateRequestDetails$.subscribe(details => {
      this.buildKandidaatForm(details);
    });
  }

  public buildKandidaatForm(candidateDetails: any): void {
    this.kandidaatForm = this.formBuilder.group({
      voornaam: [candidateDetails.Candidate.Firstname, Validators.required],
      achternaam: [candidateDetails.Candidate.Lastname, Validators.required],
      email: [candidateDetails.Candidate.Email, [Validators.required, Validators.email]],
      telefoon: [candidateDetails.Candidate.Phone, Validators.required],
      adres: [candidateDetails.Candidate.Address, Validators.required],
      selectedDegrees: [
        candidateDetails.Candidate.CandidateDegrees
          .map(candidateDegree => candidateDegree.Degree.toString())
      ],
      postcode: [candidateDetails.Candidate.PostalCode, Validators.required],
      stad: [candidateDetails.Candidate.City, Validators.required],
      werkervaring: [candidateDetails.Candidate.WorkExperience, Validators.required],
      identiteitskaart: [candidateDetails.Candidate.CardNumber, Validators.required],
      rijksregister: [candidateDetails.Candidate.RegistryNumber, Validators.required]
    });
  }

  private get emailErrorMessage(): string {
    return this.kandidaatForm.controls.email.hasError('required') ? this.requiredErrorMessage :
           this.kandidaatForm.controls.email.hasError('email') ? 'Geen geldig e-mailadres' :
           '';
  }

  private get requiredErrorMessage() {
    return 'Dit veld mag niet leeg zijn';
  }
}
