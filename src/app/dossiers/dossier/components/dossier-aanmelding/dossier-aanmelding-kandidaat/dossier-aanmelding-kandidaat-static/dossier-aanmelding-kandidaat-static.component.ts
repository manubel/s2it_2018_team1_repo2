import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Degree } from '../../../../../../common/models/degree';

@Component({
  selector: 'dossier-aanmelding-kandidaat-static',
  templateUrl: './dossier-aanmelding-kandidaat-static.component.html',
  styleUrls: ['./dossier-aanmelding-kandidaat-static.component.scss']
})
export class DossierAanmeldingKandidaatStaticComponent implements OnInit {

  candidateDegrees: any;
  candidateDetails: any;
  
  @Input() candidateRequestDetails$: Observable<any>;
  @Input() editing$: Observable<boolean>;

  constructor() { }

  ngOnInit() {
    this.candidateRequestDetails$.subscribe(details => {
      this.candidateDetails = details;
      this.candidateDegrees = details.Candidate.CandidateDegrees
      .map(candidateDegree => Degree[candidateDegree.Degree])
      .join(', ');
    });
  }

}
