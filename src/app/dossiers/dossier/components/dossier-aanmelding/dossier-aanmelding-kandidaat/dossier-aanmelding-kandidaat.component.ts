import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Observable ,  Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import * as _ from 'lodash';
import { SnackbarMessageComponent } from '../../../../../common/components/snackbar-message/snackbar-message.component';
import { DossierAanmeldingKandidaatEditableComponent } from './dossier-aanmelding-kandidaat-editable/dossier-aanmelding-kandidaat-editable.component';
import { DossierService } from '../../../services/dossier.service';

@Component({
  selector: 'dossier-aanmelding-kandidaat',
  templateUrl: './dossier-aanmelding-kandidaat.component.html',
  styleUrls: ['./dossier-aanmelding-kandidaat.component.css']
})
export class DossierAanmeldingKandidaatComponent implements OnInit {

  editing$: Subject<boolean> = new Subject();
  candidateDetails: any;
  viewIsInitted: boolean;

  @Input() candidateRequestDetails$: Observable<any>;
  @Input() candidateRequestDetailsHasError$: Observable<boolean>;

  @ViewChild(DossierAanmeldingKandidaatEditableComponent) editableComponent: DossierAanmeldingKandidaatEditableComponent;

  constructor(private snackBar: MatSnackBar, private dossierService: DossierService) {

  }

  ngOnInit() {
    this.candidateRequestDetails$.subscribe(details => this.candidateDetails = details);
  }

  cancel() {
    this.editableComponent.buildKandidaatForm(this.candidateDetails);
    this.editing$.next(false);
  }

  saveForm() {
    const formModel = this.editableComponent.kandidaatForm.value;
    this.mapFormValuesToCandidateDetails(formModel, this.candidateDetails);
    this.dossierService.saveCandidate(this.candidateDetails.Candidate).subscribe(
      response => {
        this.snackBar.openFromComponent(SnackbarMessageComponent, {
          data: { message: 'Succesvol opgeslaan', style: 'success' },
          duration: 3000
        });
        this.editing$.next(false);
        this.dossierService.updateDossierOverzicht(this.candidateDetails);
      }, error => {
        console.error(error);
        this.snackBar.openFromComponent(SnackbarMessageComponent, {
          data: { message: 'Er is iets mis gelopen bij het opslaan', style: 'danger' },
          duration: 5000
        });
      }
    );
  }

  private mapFormValuesToCandidateDetails(formModel: any, candidateDetails: any): void {
    candidateDetails.Candidate.Firstname = formModel.voornaam as string;
    candidateDetails.Candidate.Lastname = formModel.achternaam as string;
    candidateDetails.Candidate.Email = formModel.email as string;
    candidateDetails.Candidate.Phone = formModel.telefoon as string;
    candidateDetails.Candidate.Address = formModel.adres as string;
    candidateDetails.Candidate.PostalCode = formModel.postcode as string;
    candidateDetails.Candidate.City = formModel.stad as string;
    candidateDetails.Candidate.WorkExperience = formModel.werkervaring as number;
    candidateDetails.Candidate.CardNumber = formModel.identiteitskaart as string;
    candidateDetails.Candidate.RegistryNumber = formModel.rijksregister as string;
    this.mapSelectedDegreesToCandidateDegrees(formModel.selectedDegrees as Array<string>, candidateDetails);
  }

  private mapSelectedDegreesToCandidateDegrees(selectedDegrees: Array<string>, candidateDetails: any) {
    let candidateDegrees = new Array<any>();
    for (let i = 1; i <= selectedDegrees.length; i++) {
      candidateDegrees.push({ Id: i, Degree: Number.parseInt(selectedDegrees[i-1]) });
    }
    candidateDetails.Candidate.CandidateDegrees = candidateDegrees;
  }

  showFormHasErrorsSnackBar() {
    this.snackBar.openFromComponent(SnackbarMessageComponent, {
      data: { message: 'Kan niet opslaan. Er is data foutief/niet ingevuld.', style: 'warning' },
      duration: 3000
    });
  }
}
