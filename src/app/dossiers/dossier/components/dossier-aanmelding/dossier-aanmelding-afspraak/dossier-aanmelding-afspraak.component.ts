import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable ,  Subject } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { SnackbarMessageComponent } from '../../../../../common/components/snackbar-message/snackbar-message.component';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { Moment } from 'moment';
import * as moment from 'moment';
import { DossierService } from '../../../services/dossier.service';

@Component({
  selector: 'dossier-aanmelding-afspraak',
  templateUrl: './dossier-aanmelding-afspraak.component.html',
  styleUrls: ['./dossier-aanmelding-afspraak.component.css']
})
export class DossierAanmeldingAfspraakComponent implements OnInit {

  editing$: Subject<boolean> = new Subject();
  editing: boolean = false;
  dateTime: string;
  candidateDetails: any;
  afspraakForm: FormGroup

  @Input() candidateRequestDetails$: Observable<any>;
  @Input() candidateRequestDetailsHasError$: Observable<boolean>;

  constructor(private formBuilder: FormBuilder, private dossierService: DossierService, private snackBar: MatSnackBar, private dateTimeAdapter: DateTimeAdapter<any>) { 
    
  }

  ngOnInit() {
    this.candidateRequestDetails$.subscribe(details => {
      if (details) {
        this.candidateDetails = details;
        this.buildAfspraakForm(details);
      }
    });
    this.editing$.subscribe(editing => this.editing = editing);
  }

  cancel() {
    this.buildAfspraakForm(this.candidateDetails);
    this.editing$.next(false);
    this.editing = false;
  }
  
  buildAfspraakForm(candidateDetails: any): void {
    this.afspraakForm = this.formBuilder.group({
      datumAfspraak: [moment.utc(candidateDetails.AppointmentDate), Validators.required], 
      evcBegeleider: [candidateDetails.EVC_Mentor, Validators.required]
    });
  }

  saveForm() {
    const formModel = this.afspraakForm.value;
    let appointment = this.mapFormValuesToAppointment(formModel, this.candidateDetails);
    this.dossierService.saveCandidateRequestAppointment(appointment).subscribe(
      response => {
        this.snackBar.openFromComponent(SnackbarMessageComponent, {
          data: { message: 'Succesvol opgeslaan', style: 'success' },
          duration: 3000
        });
        this.candidateDetails.AppointmentDate = appointment.AppointmentDate;
        this.candidateDetails.EVC_Mentor = appointment.EVC_Mentor;
        this.editing$.next(false);
        this.editing = false;
        this.dossierService.updateDossierOverzicht(this.candidateDetails);
      }, error => {
        console.error(error);
        this.snackBar.openFromComponent(SnackbarMessageComponent, {
          data: { message: 'Er is iets mis gelopen bij het opslaan', style: 'danger' },
          duration: 5000
        });
      }
    );
  }

  private mapFormValuesToAppointment(formModel: any, candidateDetails: any): any {
    let appointmentDate = formModel.datumAfspraak as Moment;
    let evcMentor = formModel.evcBegeleider as string;
    let candidateRequestId = candidateDetails.Id;
    var appointmentDateWithoutTimezone = moment.utc([appointmentDate.year(), appointmentDate.month(), appointmentDate.date(), appointmentDate.hour(), appointmentDate.minute()]).format();

    return { 
      CandidateRequestId: candidateRequestId,
      AppointmentDate: appointmentDateWithoutTimezone,
      EVC_Mentor: evcMentor
    }
  }

  showFormHasErrorsSnackBar() {
    this.snackBar.openFromComponent(SnackbarMessageComponent, {
      data: { message: 'Kan niet opslaan. Er is data foutief/niet ingevuld.', style: 'warning' },
      duration: 3000
    });
  }

  get requiredErrorMessage() {
    return "Dit veld mag niet leeg zijn";
  } 

  get dateTimeErrorMessage(): string {
    return this.afspraakForm.controls.datumAfspraak.hasError('owlDateTimeParse') ? 'Datum is niet in het juiste formaat (dd/mm/yyyy hh:mm)' :
           this.afspraakForm.controls.datumAfspraak.hasError('required') ? this.requiredErrorMessage :
           '';
  }
}
