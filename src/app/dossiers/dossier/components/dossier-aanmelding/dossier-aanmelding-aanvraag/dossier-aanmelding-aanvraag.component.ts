import { Component, OnInit, Input } from '@angular/core';
import { Subject ,  Observable } from 'rxjs';
import { School } from '../../../../../common/models/school';

@Component({
  selector: 'dossier-aanmelding-aanvraag',
  templateUrl: './dossier-aanmelding-aanvraag.component.html',
  styleUrls: ['./dossier-aanmelding-aanvraag.component.css']
})
export class DossierAanmeldingAanvraagComponent implements OnInit {

  editing$: Subject<boolean> = new Subject();

  @Input() candidateRequestDetails$: Observable<any>;
  @Input() candidateRequestDetailsHasError$: Observable<any>;

  constructor() { }

  ngOnInit() {

  }

  annuleer() {
    this.editing$.next(false);
  }

  getSchool(enumValue: number): string {
    return School[enumValue];
  }

  getCandidateEducationCourses(candidateEducationCourses: Array<any>): string {
    return candidateEducationCourses.map(course => course.EducationCourse.Name).join(', ');
  }
}
