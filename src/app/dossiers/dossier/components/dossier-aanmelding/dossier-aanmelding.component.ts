import { Component, OnInit } from '@angular/core';
import { BehaviorSubject ,  Subject } from 'rxjs';
import { DossierService } from '../../services/dossier.service';
import { CurrentDossierService } from '../../services/current-dossier.service';

@Component({
  selector: 'dossier-aanmelding',
  templateUrl: './dossier-aanmelding.component.html',
  styleUrls: ['./dossier-aanmelding.component.css']
})
export class DossierAanmeldingComponent implements OnInit {

  candidateRequestDetailsHasError$: Subject<boolean> = new Subject();
  candidateRequestDetails$: BehaviorSubject<any> = new BehaviorSubject(null);

  constructor(private dossierService: DossierService, private currentDossierService: CurrentDossierService) { }

  ngOnInit() {
    let dossierId = this.currentDossierService.getDossierId();
    this.dossierService.getCandidateRequestDetails(dossierId)
      .subscribe(
        candidateRequestDetails => {
          this.candidateRequestDetailsHasError$.next(false);
          this.candidateRequestDetails$.next(candidateRequestDetails);
        },
        error => {
          this.candidateRequestDetailsHasError$.next(true);
          console.error(error);
    });
  }
}
