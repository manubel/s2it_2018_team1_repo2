import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DossierComponent } from './dossier.component';
import { DossierAanmeldingComponent } from './components/dossier-aanmelding/dossier-aanmelding.component';

const dossierRoutes: Routes = [
  { path: '', component: DossierComponent, children: [
    { path: '', redirectTo: 'aanmelding', pathMatch: 'full' },
    { path: 'aanmelding', component: DossierAanmeldingComponent }
   /*  { path: 'intake', component: DossierAanmeldingComponent }
    { path: 'documenteren', component: DossierAanmeldingComponent } */
  ]}
];

@NgModule({
  imports: [
    RouterModule.forChild(dossierRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class DossierRoutingModule { }
