
import {takeUntil} from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject ,  BehaviorSubject } from 'rxjs';
import { DossierService } from './services/dossier.service';
import { CurrentDossierService } from './services/current-dossier.service';


@Component({
  selector: 'dossier',
  templateUrl: './dossier.component.html',
  styleUrls: ['./dossier.component.scss'],
  providers: [CurrentDossierService]
})
export class DossierComponent implements OnInit, OnDestroy {

  unSubscriber$: Subject<boolean> = new Subject();
  candidateRequestDetailsHasError$: Subject<boolean> = new Subject();
  candidateRequestDetails$: BehaviorSubject<any> = new BehaviorSubject(null);
  
  constructor(private dossierService: DossierService, private currentDossierService: CurrentDossierService) { }
  
  ngOnInit() {
    let dossierId = this.currentDossierService.getDossierId();
    this.dossierService.getCandidateRequestDetails(dossierId).subscribe(candidateRequestDetails => {
      this.candidateRequestDetailsHasError$.next(false);
      this.candidateRequestDetails$.next(candidateRequestDetails);
    },
    error => {
      this.candidateRequestDetailsHasError$.next(true);
      console.error(error);
    });
    
    this.dossierService.candidateRequestDetails$.pipe(
    takeUntil(this.unSubscriber$))
    .subscribe(candidateRequestDetails => this.candidateRequestDetails$.next(candidateRequestDetails));
  }

  ngOnDestroy(): void {
    this.unSubscriber$.next(true);
    this.unSubscriber$.complete();
  }
}
