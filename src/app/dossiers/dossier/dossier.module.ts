//#region PROVIDERS
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DossierRoutingModule } from './dossier-routing.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EvcCommonModule } from '../../common/evc-common.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OwlDateTimeModule, OwlDateTimeIntl, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment';
//#endregion PROVIDERS
//#region DECLARATIONS
import { DossierComponent } from './dossier.component';
import { DossierAanmeldingComponent } from './components/dossier-aanmelding/dossier-aanmelding.component';
import { DossierOverzichtComponent } from './components/common/dossier-overzicht/dossier-overzicht.component';
import { DossierAanmeldingKandidaatComponent } from './components/dossier-aanmelding/dossier-aanmelding-kandidaat/dossier-aanmelding-kandidaat.component';
import { DossierAanmeldingKandidaatEditableComponent } from './components/dossier-aanmelding/dossier-aanmelding-kandidaat/dossier-aanmelding-kandidaat-editable/dossier-aanmelding-kandidaat-editable.component';
import { DossierAanmeldingKandidaatStaticComponent } from './components/dossier-aanmelding/dossier-aanmelding-kandidaat/dossier-aanmelding-kandidaat-static/dossier-aanmelding-kandidaat-static.component';
import { DossierAanmeldingAanvraagComponent } from './components/dossier-aanmelding/dossier-aanmelding-aanvraag/dossier-aanmelding-aanvraag.component';
import { DossierAanmeldingAfspraakComponent } from './components/dossier-aanmelding/dossier-aanmelding-afspraak/dossier-aanmelding-afspraak.component';
//#endregion DECLARATIONS
//#region PROVIDERS
import { DossierService } from './services/dossier.service';
import { CurrentDossierService } from './services/current-dossier.service';
import { CustomDateTimePickerLabels, CUSTOM_MOMENT_FORMATS } from '../models/date-time-picker-settings';
//#endregion PROVIDERS

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DossierRoutingModule,
    MatTooltipModule,
    EvcCommonModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatSnackBarModule,
    OwlDateTimeModule,
    OwlMomentDateTimeModule
  ],
  declarations: [
    DossierComponent,
    DossierAanmeldingComponent,
    DossierOverzichtComponent,
    DossierAanmeldingKandidaatComponent,
    DossierAanmeldingKandidaatEditableComponent,
    DossierAanmeldingKandidaatStaticComponent,
    DossierAanmeldingAanvraagComponent,
    DossierAanmeldingAfspraakComponent
  ],
  providers: [
    DossierService,
    CurrentDossierService,
    { provide: OWL_DATE_TIME_LOCALE, useValue: 'nl' },
    { provide: OwlDateTimeIntl, useClass: CustomDateTimePickerLabels },
    { provide: OWL_DATE_TIME_FORMATS, useValue: CUSTOM_MOMENT_FORMATS }
  ]
})
export class DossierModule { }
