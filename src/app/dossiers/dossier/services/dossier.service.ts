import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  Subscriber ,  Subject } from 'rxjs';

@Injectable()
export class DossierService {

  private candidateRequestDetails: Subject<any> = new Subject();
  public candidateRequestDetails$: Observable<any> = this.candidateRequestDetails.asObservable();

  constructor(private http: HttpClient) { }

  public getCandidateRequestDetails(id: string): Observable<any> {
    return this.http.get<string>(`/api/Requests/${id}`);
  }

  public saveCandidate(candidateDetails): Observable<any> {
    return this.http.post('/api/Candidates', candidateDetails);
  }

  public saveCandidateRequestAppointment(appointment): Observable<any> {
    return this.http.put('/api/Requests/Appointment', appointment);
  }

  public updateDossierOverzicht(candidateRequestDetails: any): void {
    this.candidateRequestDetails.next(candidateRequestDetails);
  }
}
