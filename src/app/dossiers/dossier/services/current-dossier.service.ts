import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Injectable()
export class CurrentDossierService {

  constructor(private activatedRoute: ActivatedRoute) { }

  public getDossierId(): string {
    return this.activatedRoute.snapshot.params['dossierId'];
  }

}
