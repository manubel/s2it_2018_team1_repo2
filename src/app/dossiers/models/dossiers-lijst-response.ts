
export interface DossiersLijstResponse {
  id: string;
  dossiernummer: string;
  voornaam: string;
  achternaam: string;
  registratiedatum: string;
  status: string;
  school: string;
}
