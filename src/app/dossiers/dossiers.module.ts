//#region PROVIDERS
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DossiersRoutingModule } from './dossiers-routing.module';
import { MatTooltipModule } from '@angular/material/tooltip';
import { EvcCommonModule } from '../common/evc-common.module';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
//#endregion PROVIDERS
//#region DECLARATIONS
import { DossiersLijstComponent } from './components/dossiers-lijst/dossiers-lijst.component';
import { DossiersLijstTabelComponent } from './components/dossiers-lijst/dossiers-lijst-tabel/dossiers-lijst-tabel.component';
//#endregion DECLARATIONS
//#region PROVIDERS
import { DossiersService } from './services/dossiers.service';
import {MatPaginatorIntl, MatPaginatorModule, MatSortModule, MatTableModule} from '@angular/material';
import { DossiersLijstService } from './services/dossiers-lijst.service';
import {DutchTablePaginator} from '../common/classes/dutch-table-paginator';
//#endregion PROVIDERS

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DossiersRoutingModule,
    MatTooltipModule,
    EvcCommonModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatSnackBarModule
  ],
  declarations: [
    DossiersLijstComponent,
    DossiersLijstTabelComponent,
  ],
  providers: [
    DossiersService,
    DossiersLijstService,
    {provide: MatPaginatorIntl, useClass: DutchTablePaginator}
  ]
})
export class DossiersModule { }
