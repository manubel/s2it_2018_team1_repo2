import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimalComma'
})
export class DecimalCommaPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if (!value) return "";

    return value.toString().replace('.', ',');
  }

}
