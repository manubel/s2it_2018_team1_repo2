import { Directive, Input, HostListener, ElementRef, AfterViewChecked } from '@angular/core';

@Directive({
  selector: '[matchWidth]'
})
export class MatchWidthDirective implements AfterViewChecked {

  @Input() matchWidth: string; 

  constructor(private el: ElementRef) { }

  @HostListener('window:resize')
  onResize() {
    this.matchWidthOfChildClasses(this.el.nativeElement, this.matchWidth);
  }
  
  ngAfterViewChecked(): void {
    this.matchWidthOfChildClasses(this.el.nativeElement, this.matchWidth);
  }

  matchWidthOfChildClasses(parent: HTMLElement, className: string) {
    if (!parent) return;

    const children = parent.getElementsByClassName(className);
    if (!children) return;

    Array.from(children).forEach((childElement: HTMLElement) => childElement.style.width = 'initial');

    const itemWidths = Array.from(children).map(childElement => childElement.getBoundingClientRect().width);

    const maxWidth = itemWidths.reduce((prev, curr) => {
      return curr > prev ? curr : prev;
    }, 0);

    Array.from(children).forEach((childElement: HTMLElement) => childElement.style.width = `${maxWidth}px`);
  }
}
