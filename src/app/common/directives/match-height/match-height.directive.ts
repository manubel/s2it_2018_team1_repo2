import { Directive, Input, ElementRef, HostListener, AfterContentChecked, AfterViewChecked } from '@angular/core';

@Directive({
  selector: '[matchHeight]'
})

/** Dit is een directive dat de hoogte van de meegegeven elementen even hoog gaat maken. 
 * Geef als input een array van cssSelectors. De grootst gevonden hoogte wordt de hoogte van alle meegegeven elementen.
 */
export class MatchHeightDirective implements AfterViewChecked {


  @Input() matchHeight: Array<string>;


  constructor(private el: ElementRef) { }

  @HostListener('window:resize')
  onResize() {
    this.matchHeightOfChildClasses(this.el.nativeElement, this.matchHeight);
  }
  
  ngAfterViewChecked(): void {
    this.matchHeightOfChildClasses(this.el.nativeElement, this.matchHeight);
  }


  matchHeightOfChildClasses(parent: HTMLElement, elementSelectors: Array<string>) {
    if (!parent) { return; }

    let children = new Array<HTMLElement>();
    elementSelectors.forEach(selector => children.push(parent.querySelector(selector) as HTMLElement));

    if (!children) return;
    children = children.filter(child => child != null);
    if (children.every(child => child == null)) return;
    console.log(children);

    Array.from(children).forEach((childElement: HTMLElement) => childElement.style.height = 'initial');

    const itemHeights = Array.from(children).map(childElement => childElement.getBoundingClientRect().height);
    const maxHeight = itemHeights.reduce((prev, curr) => {
      return curr > prev ? curr : prev;
    }, 0);

    Array.from(children).forEach((childElement: HTMLElement) => childElement.style.height = `${maxHeight}px`);
  }
}
