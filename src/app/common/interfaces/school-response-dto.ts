import {School} from '../models/school';

export interface SchoolResponseDto {
  EVC_Coordinator: EvcCoordinatorDto;
  Educations: EducationsDto[];
}

export interface EvcCoordinatorDto {
  Id: number;
  Firstname: string;
  Lastname: string;
  Email: string;
  School: School;
}

export interface EducationsDto {
  Id: number;
  Name: string;
  SchoolYear: Date;
  School: School;
  EducationCourses: EducationCoursesDto[];
}

export interface EducationCoursesDto {
  Id: number;
  Name: string;
  EducationYear: number;
}

