import {EducationCoursesDto} from './school-response-dto';

export interface TrajectenDto {
  trajectschijf1: EducationCoursesDto[];
  trajectschijf2: EducationCoursesDto[];
  trajectschijf3: EducationCoursesDto[];
}
