import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable ,  Subject ,  BehaviorSubject } from 'rxjs';

@Injectable()
export class LoginService {

  public userIsLoggedIn = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  public loginUser() {
    this.userIsLoggedIn.next(true);
  }

  public logoutUser() {
    this.userIsLoggedIn.next(false);
  }

}
