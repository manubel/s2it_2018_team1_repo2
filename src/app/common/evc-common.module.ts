//#region IMPORTS
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
//#endregion IMPORTS
//#region DECLARATIONS
import { NavbarTopComponent } from './components/navbar-top/navbar-top.component';
import { ProgressTrackerComponent } from './components/progress-tracker/progress-tracker.component';
import { LogosFooterComponent } from './components/logos-footer/logos-footer.component';
import { SnackbarMessageComponent } from './components/snackbar-message/snackbar-message.component';
import { ErrorMessageComponent } from './components/error-message/error-message.component';
import { LoadingBlocksComponent } from './components/loading-blocks/loading-blocks.component';
import { CardTitleEditableComponent } from './components/card-title-editable/card-title-editable.component';
import { MatchHeightDirective } from './directives/match-height/match-height.directive';
import { MatchWidthDirective } from './directives/match-width/match-width.directive';
import { EnumPipe } from './pipes/enum/enum.pipe';
import { DecimalCommaPipe } from './pipes/decimal-comma/decimal-comma.pipe';
import { DateFormatPipe } from './pipes/date-format/date-format.pipe';
//#endregion DECLARATIONS
//#region PROVIDERS
import { LoginService } from './services/login/login.service';
//#endregion PROVIDERS

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatTooltipModule
  ],
  declarations: [
    NavbarTopComponent,
    ProgressTrackerComponent,
    LogosFooterComponent,
    SnackbarMessageComponent,
    ErrorMessageComponent,
    LoadingBlocksComponent,
    CardTitleEditableComponent,
    MatchHeightDirective,
    MatchWidthDirective,
    EnumPipe,
    DecimalCommaPipe,
    DateFormatPipe
  ],
  providers: [
    LoginService,
  ],
  exports: [
    NavbarTopComponent,
    ProgressTrackerComponent,
    LogosFooterComponent,
    ErrorMessageComponent,
    LoadingBlocksComponent,
    CardTitleEditableComponent,
    MatchHeightDirective,
    MatchWidthDirective,
    EnumPipe,
    DecimalCommaPipe,
    DateFormatPipe
  ],
  entryComponents: [
    SnackbarMessageComponent
  ]
})
export class EvcCommonModule { }
