export enum Degree {
    'Secundair onderwijs' = 1,
    'Professionele bachelor',
    'Academische bachelor',
    'Master',
    'Geen diploma'
}
