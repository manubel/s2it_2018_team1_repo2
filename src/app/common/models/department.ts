export enum Department {
    PXL_Business = 1,
    PXL_Education = 2,
    PXL_Healthcare = 3,
    PXL_IT = 4,
    PXL_MAD = 5,
    PXL_MediaTourisme = 6,
    PXL_Music = 7,
    PXL_SocialWork = 8,
    PXL_Tech = 9
}