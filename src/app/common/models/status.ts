export enum Status {
  'Aanmelden' = 1,
  'Intake',
  'Afgekeurd kandidaat',
  'VI',
  'Afgekeurd VI',
  'Documenteren'
}
