import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login/login.service';

@Component({
  selector: 'navbar-top',
  templateUrl: './navbar-top.component.html',
  styleUrls: ['./navbar-top.component.css']
})
export class NavbarTopComponent implements OnInit {

  coordinatorIsIngelogd: boolean = false;
  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.loginService.userIsLoggedIn.subscribe(isLoggedIn => this.coordinatorIsIngelogd = isLoggedIn);
  }

  logInUser() {
    this.loginService.loginUser();
  }
  
  logoutUser() {
    this.loginService.logoutUser();
  }

}
