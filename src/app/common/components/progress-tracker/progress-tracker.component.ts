import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'progress-tracker',
  templateUrl: './progress-tracker.component.html',
  styleUrls: ['./progress-tracker.component.css']
})
export class ProgressTrackerComponent implements OnInit {

  isInVerslagFase = true;
  isInEvaluatieFase = true;

  constructor() { }

  ngOnInit() {
  }

  setVisibilityVerslag(): string {
    return this.isInVerslagFase ? 'visible' : 'hidden';
  }

  setVisibilityEvaluatie(): string {
    return this.isInEvaluatieFase ? 'visible' : 'hidden';
  }

}
