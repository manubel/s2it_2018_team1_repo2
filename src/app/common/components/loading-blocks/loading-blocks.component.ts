import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'loading-blocks',
  templateUrl: './loading-blocks.component.html',
  styleUrls: ['./loading-blocks.component.scss']
})

/* Voor verschillende loading templates, zie: http://tobiasahlin.com/spinkit/  */
export class LoadingBlocksComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
