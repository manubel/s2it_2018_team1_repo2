import { Component, OnInit, Input } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'card-title-editable',
  templateUrl: './card-title-editable.component.html',
  styleUrls: ['./card-title-editable.component.scss']
})
export class CardTitleEditableComponent implements OnInit {

  @Input() cardTitle: string;
  @Input() editing$: Subject<boolean>;
  @Input() canEdit: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  edit() {
    if (this.canEdit) this.editing$.next(true);
  }

  cancel() {
    this.editing$.next(false);
  }

}
