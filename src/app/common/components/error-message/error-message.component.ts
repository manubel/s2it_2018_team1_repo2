import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss']
})
export class ErrorMessageComponent implements OnInit {

  @Input() title: string;
  @Input() message: string;
  @Input() theme: string = "dark";

  constructor() { }

  ngOnInit() {
  }

  get inputTheme(): string {
    return `bg-${this.theme}`;
  }

}
