import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {EducationsDto, SchoolResponseDto} from '../../common/interfaces/school-response-dto';

@Injectable()
export class EducationService {


  constructor(private http: HttpClient) { }

  public getSchool(schoolid: number): Observable<SchoolResponseDto> {
    return this.http
      .get<SchoolResponseDto>('/api/requests/school/' + schoolid);
  }

  public getOpleidingen(schoolid: number): Observable<EducationsDto[]> {
    return this.http.get<EducationsDto[]>('/api/requests/school/' + schoolid + '/educations');
  }

  public postAanmelding(body): Observable<any> {
    return this.http.post('/api/requests', body);
  }
}
