import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {EducationService} from '../../services/education.service';
import {Observable} from 'rxjs';
import {startWith, map} from 'rxjs/operators';
import {EducationsDto, SchoolResponseDto} from '../../../common/interfaces/school-response-dto';
import {TrajectenDto} from '../../../common/interfaces/traject-dto';
import {School} from '../../../common/models/school';
import {MatSelect, MatSnackBar} from '@angular/material';
import {Degree} from '../../../common/models/degree';
import {SnackbarMessageComponent} from '../../../common/components/snackbar-message/snackbar-message.component';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})


export class RegisterComponent implements OnInit {
  @ViewChild('trajectenSelect') trajectenSelect: MatSelect;
  scholen = School;
  selectedSchool: number;
  evcCoordinator: string;
  evcCoordinatorId: number;
  selectedOpleidingsOnderdelen: number[];
  schoolResponse: Observable<SchoolResponseDto>;
  educationsResponseObservable: Observable<EducationsDto[]>;
  educationResponseUnedited: EducationsDto[];
  trajecten: TrajectenDto;

  degrees = Degree;

  opleidingFormControl = new FormControl('', Validators.required);

  kandidaatForm: FormGroup;
  kandidaatPost = {};

  constructor(private _educationService: EducationService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
  }

  private get emailErrorMessage(): string {
    return this.kandidaatForm.controls.email.hasError('required') ? this.requiredErrorMessage :
      this.kandidaatForm.controls.email.hasError('email') ? 'Geen geldig e-mailadres' :
        '';
  }

  private get requiredErrorMessage() {
    return 'Dit veld mag niet leeg zijn';
  }

  ngOnInit() {
    this.trajectenSelect.setDisabledState(true);
    this.buildKandidaatForm();
  }

  buildKandidaatForm(): void {
    this.kandidaatForm = this.formBuilder.group({
      voornaam: ['', Validators.required],
      achternaam: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      telefoon: ['', Validators.required],
      adres: ['', Validators.required],
      postcode: ['', Validators.required],
      stad: ['', Validators.required],
      werkervaring: ['', Validators.required],
      identiteitskaart: ['', Validators.required],
      rijksregister: ['', Validators.required],
      diploma: ['', Validators.required]
    });
  }

  setOpleidingen() {
    if (this.educationResponseUnedited) {
      this.trajectenSelect.setDisabledState(true);
      this.selectedOpleidingsOnderdelen = [];
      this.opleidingFormControl.reset('');
    }

    this.schoolResponse = this._educationService.getSchool(this.selectedSchool);

    this.schoolResponse.subscribe(response => {
      this.educationResponseUnedited = response.Educations;
      this.setEvcCoordinatorNaam(response);
      this.setFiltersOnEducationsObservable();
    });
  }

  // Trajecten sorteren voor de dropdown
  toonTrajecten() {
    this.trajectenSelect.setDisabledState(false);
    const opleiding = this.opleidingFormControl.value;
    const schijf1 = opleiding.EducationCourses
      .filter(value => value.EducationYear === 1);
    const schijf2 = opleiding.EducationCourses
      .filter(value => value.EducationYear === 2);
    const schijf3 = opleiding.EducationCourses
      .filter(value => value.EducationYear === 3);

    this.trajecten = <TrajectenDto>{
      trajectschijf1: schijf1,
      trajectschijf2: schijf2,
      trajectschijf3: schijf3
    };
  }

  // mat-autocomplete gebruikt normaalgezien
  displayView(object?: EducationsDto): string | undefined {
    return object ? object.Name : undefined;
  }

  aanmeldingVoltooien() {
    // TODO add validatie voor 2 formcontrols + object

    const kandidaatForm = this.kandidaatForm.value;
    this.mapFormValuesToCandidateDetails(kandidaatForm, this.kandidaatPost);
    this._educationService.postAanmelding(this.kandidaatPost).subscribe(
      () => {
        this.snackBar.openFromComponent(SnackbarMessageComponent, {
          data: {message: 'Succesvol opgeslaan', style: 'success'},
          duration: 3000
        });
      }, error => {
        console.log(error);
        this.snackBar.openFromComponent(SnackbarMessageComponent, {
          data: {message: 'Er is iets mis gelopen bij het opslaan', style: 'danger'},
          duration: 5000
        });
      }
    );
  }

  private mapSelectedDegreesToCandidateDegrees(selectedDegrees: Array<string>, candidateDetails: any): void {
    const candidateDegrees = [];
    for (let i = 0; i < selectedDegrees.length; i++) {
      candidateDegrees.push({Degree: Number.parseInt(selectedDegrees[i])});
    }

    candidateDetails.Candidate.CandidateDegrees = candidateDegrees;
  }

  private mapEducationCourses(selectedEducationCourses: Array<string>, post: any): void {
    const educationCourses = [];
    for (let i = 0; i < selectedEducationCourses.length; i++) {
      educationCourses.push({Id: selectedEducationCourses[i]});
    }

    post.EducationCourses = educationCourses;
  }

  private setFiltersOnEducationsObservable() {
    this.educationsResponseObservable = this.opleidingFormControl.valueChanges.pipe(
      startWith(''),
      map(value => this.educationsFilter(value)));
  }

  private setEvcCoordinatorNaam(response) {
    this.evcCoordinatorId = response.EVC_Coordinator.Id;
    this.evcCoordinator = response.EVC_Coordinator.Firstname + ' ' + response.EVC_Coordinator.Lastname;
  }

  private educationsFilter(value: any) {
    return this.educationResponseUnedited.filter(option => {
      if (typeof value !== 'string') {
        value = value.Name;
      }
      return option.Name.toLowerCase().indexOf(value.toLowerCase()) === 0;
    });
  }

  private mapFormValuesToCandidateDetails(formModel: any, post: any): void {
    post.Candidate = {};
    post.Candidate.Firstname = formModel.voornaam as string;
    post.Candidate.Lastname = formModel.achternaam as string;
    post.Candidate.Email = formModel.email as string;
    post.Candidate.Phone = formModel.telefoon as string;
    post.Candidate.Address = formModel.adres as string;
    post.Candidate.PostalCode = formModel.postcode as string;
    post.Candidate.City = formModel.stad as string;
    post.Candidate.WorkExperience = formModel.werkervaring as number;
    post.Candidate.CardNumber = formModel.identiteitskaart as string;
    post.Candidate.RegistryNumber = formModel.rijksregister as string;
    post.PrivacyAgreed = true;

    post.RegisterDate = new Date();
    post.Paid = false;
    post.School = this.selectedSchool;
    post.EducationId = this.opleidingFormControl.value.Id;
    post.EVC_CoordinatorId = this.evcCoordinatorId;
    this.mapEducationCourses(this.selectedOpleidingsOnderdelen as Array<any>, post);
    this.mapSelectedDegreesToCandidateDegrees(formModel.diploma as Array<any>, post);
  }
}
