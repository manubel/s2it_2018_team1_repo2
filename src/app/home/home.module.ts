//#region IMPORTS
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EvcCommonModule} from '../common/evc-common.module';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
//#endregion IMPORTS
//#region DECLARATIONS
import {HomeComponent} from './components/home/home.component';
import {HomeCommonComponent} from './components/common/home-common/home-common.component';
import {ContactComponent} from './components/contact/contact.component';
import {LoginComponent} from './components/login/login.component';
import {HomeTemplateComponent} from './components/home-template.component';
import {RegisterComponent} from './components/register/register.component';
import {HomeRoutingModule} from './home-routing.module';
import {EducationService} from './services/education.service';
import {NgxMatSelectSearchModule} from 'ngx-mat-select-search';
import {
  MatAutocompleteModule,
  MatButtonModule, MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatListModule,
  MatOptionModule, MatSelectModule,
  MatSnackBarModule
} from '@angular/material';

//#endregion DECLARATIONS

@NgModule({
  imports: [
    CommonModule,
    EvcCommonModule,
    RouterModule,
    FormsModule,
    HomeRoutingModule,
    NgxMatSelectSearchModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatListModule,
    MatOptionModule,
    MatCheckboxModule,
    MatSnackBarModule
  ],
  declarations: [
    HomeComponent,
    HomeCommonComponent,
    ContactComponent,
    LoginComponent,
    HomeTemplateComponent,
    RegisterComponent
  ],
  providers: [
    EducationService
  ]
})
export class HomeModule {
}
