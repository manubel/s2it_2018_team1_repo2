//#region IMPORTS
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { EvcCommonModule } from './common/evc-common.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//#endregion IMPORTS
//#region DECLARATIONS
import { AppComponent } from './app.component';
//#endregion DECLARATIONS
//#region PROVIDERS
import { ApiCallsInterceptor } from './common/interceptor/webapi-calls-interceptor';
//#endregion PROVIDERS

@NgModule({
  imports: [
    HttpClientModule,
    AppRoutingModule,
    RouterModule,
    BrowserModule,
    FormsModule,
    EvcCommonModule,
    BrowserAnimationsModule,
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ApiCallsInterceptor, multi: true },
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
