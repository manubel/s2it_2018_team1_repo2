export const environment = {
  production: true,
  apiUrl: 'http://evcregistrationwebapi.azurewebsites.net',
  securedApiUrl: 'https://evcregistrationwebapi.azurewebsites.net'
};


// apiUrl: 'http://evc-registration-server.azurewebsites.net',
// securedApiUrl: 'https://evc-registration-server.azurewebsites.net'
